//
//  DvAppDelegate.h
//  DvView
//
//  Created by User on 13年1月25日.
//  Copyright (c) 2013年 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DvViewController;

@interface DvAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) DvViewController *viewController;

@end
